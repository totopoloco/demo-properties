package at.mavila.propsd;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "myapp.module1")
@Getter
@Setter
@ToString
@Slf4j
public class Module1Configuration extends AbstractConfiguration {
  private String another;
}
