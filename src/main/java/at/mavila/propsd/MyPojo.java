package at.mavila.propsd;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.With;

@With
@Builder
@Getter
@Setter
@ToString
public class MyPojo {
  private String key1;
  private String key2;
  private String key3;
  private String key4;
}
