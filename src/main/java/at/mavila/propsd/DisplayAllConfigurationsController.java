package at.mavila.propsd;

import java.util.Map;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DisplayAllConfigurationsController {

  private final Module1Configuration module1Configuration;
  private final Module2Configuration module2Configuration;

  public DisplayAllConfigurationsController(
      final Module1Configuration module1Configuration,
      final Module2Configuration module2Configuration) {
    this.module1Configuration = module1Configuration;
    this.module2Configuration = module2Configuration;
  }

  @GetMapping("/displayAnother")
  String displayAnother() {
    return this.module1Configuration.getAnother();
  }

  @GetMapping("/displayExtra")
  String displayExtra() {
    return this.module2Configuration.getExtra();
  }

  @GetMapping("/displayModule1")
  Map<String, MyPojo> getPropsModule1() {
    return this.module1Configuration.getStation();
  }

  @GetMapping("/displayModule2")
  Map<String, MyPojo> getPropsModule2() {
    return this.module2Configuration.getStation();
  }

}