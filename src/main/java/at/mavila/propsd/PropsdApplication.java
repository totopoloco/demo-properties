package at.mavila.propsd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PropsdApplication {

	public static void main(String[] args) {
		SpringApplication.run(PropsdApplication.class, args);
	}

}
